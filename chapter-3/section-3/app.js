/**
 * This app.js outputs the first 5 numbers in the series
 * to the console for the series which are the smallest
 * number > 1 whose representation in all bases up to 
 * n consists only of zeros and ones.
 *
 * @summary   Logs 1,2,4,82000 to console.
 *
 * @link      https://oeis.org/A258107
 * @since     1.2.1
 */
 
/**
 * Convert from any base (2 to 34) to any base (2 to 34)
 *
 * @link	http://stackoverflow.com/a/32480941
 */
function cb(value, from_base, to_base) {
  var range = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/'.split('');
  var from_range = range.slice(0, from_base);
  var to_range = range.slice(0, to_base);
  
  var dec_value = value.split('').reverse().reduce(function (carry, digit, index) {
    if (from_range.indexOf(digit) === -1) throw new Error('Invalid digit `'+digit+'` for base '+from_base+'.');
    return carry += from_range.indexOf(digit) * (Math.pow(from_base, index));
  }, 0);
  
  var new_value = '';
  while (dec_value > 0) {
    new_value = to_range[dec_value % to_base] + new_value;
    dec_value = (dec_value - (dec_value % to_base)) / to_base;
  }
  return new_value || '0';
}

// Calculate the next number in series
var next = (function () {
    var i = 0;
    return function () {
    	do{
	    	i++;
			var s = 1;
			for( b = 2; b <= Math.min(5, i); b++ ){		
				var n = cb( i.toString(), 10, b ).toString();
				for (var p = 0; p < n.length; p++) {
				  	if( 1 < n[p]){
				  		s = 0;
				  		break;
				  	}
				}
				if( !s ){
					break;
				}
			}
		}while( !s );
    	return i;
    }
})();

var output = "Sequence: ";

var sequence = new Array();

// Generate the first 5 values values
for (var i = 0, len = 5; i < len; i++) {
	sequence.push( next() );
}

// Prepare the array of values to be displayed 
for (var i = 0; i < sequence.length; i++) {
  	output +=  sequence[i];
  	if(i < sequence.length - 1){
  		output += ", ";
  	}
}

// Log the output in the console
console.log(output);
