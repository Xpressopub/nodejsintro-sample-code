// Require the HTTP module
var http = require('http');

// Require the file system module
var fs = require("fs");

//set port for all requests
const PORT=8080; 

//Function to handle all 404 requests
function handle404Request(request, response){
    response.writeHead(404);
    response.end('Error: 404, Page not found: ' + request.url);
}


//Function to handle all requests
function handleRequest(request, response){
    if(request.url == "/"){
    	fs.readFile("index.html", "binary", function(err, file) {
			response.writeHead(200);
			response.write(file, "binary");
			response.end();
		});

    }else{
    	handle404Request(request, response);
    }
}

//Setup the server
var server = http.createServer(handleRequest);

//Start our server
server.listen(PORT, function(){
    //Callback when server is accessed
    console.log("Server listening on: http://localhost:%s", PORT);
});