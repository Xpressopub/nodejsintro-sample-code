/**
 * Opens a popup window when trying to leave site
 *
 * @summary   Keeps visitor on site longer
 *
 * @link      https://nsa.gov/
 * @since     8.1.1
 */

 function popup(){
 	// begin tracking
 	console.log('open popup...');
 }
