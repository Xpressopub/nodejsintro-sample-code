//Require the HTTP module
var http = require('http');

//set port for all requests
const PORT=8080; 

//Function to handle all requests
function handleRequest(request, response){
    response.end('It Worked, path requested: ' + request.url);
}

//Setup the server
var server = http.createServer(handleRequest);

//Start our server
server.listen(PORT, function(){
    //Callback when server is accessed
    console.log("Server listening on: http://localhost:%s", PORT);
});