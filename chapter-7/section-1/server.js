// Require the HTTP module
var http = require('http');

// Require the file system module
var fs = require("fs");

// Require Query String module
var qs = require('querystring');

//set port for all requests
const PORT=8080; 

//Function to handle all 404 requests
function handle404Request(request, response){
    response.writeHead(404);
    response.end('Error: 404, Page not found: ' + request.url);
}

//Function to handle all requests
function handleRequest(request, response){
    if(request.url == "/"){
    	fs.readFile("login.html", "binary", function(err, file) {
			response.writeHead(200);
			response.write(file, "binary");
			response.end();
		});

    }else if(request.url == "/login" && request.method == 'POST'){
    	var body = '';
        request.on('data', function (data) {
            body += data;
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6) { 
                // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                request.connection.destroy();
            }
        });
        request.on('end', function () {
            // use post to access POST Values
            var post = qs.parse(body);
            // Hardcoded Username and Password
            if( 'bob' == post.username && 'password' == post.password ){
	    		fs.readFile("success.html", "binary", function(err, file) {
					response.writeHead(200);
					response.write(file, "binary");
					response.end();
				});
	    	}else{
                // Login failed, show error page
	    		fs.readFile("error.html", "binary", function(err, file) {
                    //  return header 403 permission denied
					response.writeHead(403);
					response.write(file, "binary");
					response.end();
				});	
	    	}

        });

    }else{
    	handle404Request(request, response);
    }
}

//Setup the server
var server = http.createServer(handleRequest);

//Start our server
server.listen(PORT, function(){
    //Callback when server is accessed
    console.log("Server listening on: http://localhost:%s", PORT);
});