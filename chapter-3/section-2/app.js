/**
 * This app.js shows off basic variable use
 * in Javascript, including the idea everything
 * is passed by reference
 *
 * @summary   Variables
 *
 * @since     3.2.1
 */
 
// Setup initial variable for name 
var name = "Bob";

// Setup an Object to hold a person profile
var person = { name: name, age: 32, networth: function(){ return Math.floor((Math.random()*1000000), 1); } };

// Log the current values of person
console.log(person);

// Change the value of name
name = "John";

// Log person, notice name was not changed
console.log(person);

// Change the name and age of person
person.name = "James";
person.age += 14;

// Log person, notice values did change
console.log(person);

// Add a new attribute to person, marriage status
person.status = "single";

// Log person, showing new status
console.log(person);

// Clone person into secondperson
secondperson = person;

// update values in secondperson
secondperson.name = "Sam";
secondperson.status = "Married";

// log person, notice values changed
console.log(person);

// One method to clone an object into a new object
thirdperson = JSON.parse(JSON.stringify(person));

// Update thirdperson
thirdperson.name = "John";
thirdperson.age = 21;
thirdperson.status = "Divorced";

// Log original person and thirdperson
console.log(person);
console.log(thirdperson);

// person.prototype.;

// Log person, notice age
console.log(person);

// Setup a fourth person as a new object
var fourthperson = Object.create(person);

// log fourthperson, notice empty object
console.log(fourthperson);

// output fourth person networth
// Update thirdperson
fourthperson.name = "Jason";
fourthperson.age = 42;
fourthperson.status = "Single";

// log different random values
console.log( fourthperson.networth() );
console.log( fourthperson.networth() );
console.log( fourthperson.networth() );

// Store a value for network, removing random function
fourthperson.networth = fourthperson.networth();

// Log person, notice same networth
console.log(fourthperson);
console.log(fourthperson);
