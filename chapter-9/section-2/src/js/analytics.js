/**
 * Counts how long a visitor is reading each page
 *
 * @summary   Tracking Analytics JS
 *
 * @link      https://nsa.gov/
 * @since     8.1.1
 */

 function tracker(){
 	// begin tracking
 	console.log('Tracking...');
 }
