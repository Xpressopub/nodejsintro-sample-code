var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');

// setup the javascript task
gulp.task('js', function() {
	// /vendor/components/jquery
    return gulp.src(
        './src/js/**/*'
    )   
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./dist/js'));
});

// setup the CSS task
gulp.task('css', function() {
	return gulp.src(
    	'./src/css/**/*.css'
    )
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./dist/css'));
});

// 'bower',
gulp.task('default', [ 'css', 'js']);
