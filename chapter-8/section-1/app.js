/**
 * The Bacon Ipsum JSON API is a REST interface 
 * for generating meaty lorem ipsum text and can 
 * be used by any application. Pass in the following 
 * parameters using an HTTP GET and we'll return 
 * a JSON string array of paragraphs. It can also 
 * be accessed via https.
 *
 * @summary   This app.js outputs random bacom ipsum values.
 *
 * @link      https://baconipsum.com/json-api/
 * @since     8.1.1
 */

// Require the HTTPS module
var https = require('https');
var qs = require('querystring'); 

// API Parameters:

/*
    type: all-meat for meat only or meat-and-filler for meat mixed with miscellaneous 'lorem ipsum' filler.
    paras: optional number of paragraphs, defaults to 5.
    sentences: number of sentences (this overrides paragraphs)
    start-with-lorem: optional pass 1 to start the first paragraph with 'Bacon ipsum dolor sit amet'.
    format: 'json' (default), 'text', or 'html'
*/

// '/api/?type=meat-and-filler&format=text'

var path = '/api/?' + qs.stringify({type: 'meat-and-filler', format: 'text'});

// Setup the request option values
var options = {
  host: 'baconipsum.com',
  port: 443, // 443 Secure URL, 80 Standard URL
  path: path,
  method: 'GET',
  protocol: 'https:'
};



// Begin the request
var request = https.request(options, function(res) {
  // removed, to just show body content
  // console.log('STATUS: ' + res.statusCode);
  // console.log('HEADERS: ' + JSON.stringify(res.headers));
  // res.setEncoding('utf8');
  res.on('data', function (chunk) {
    console.log('' + chunk); // empty string to cast to string
  });
});

// Handle any errors
request.on('error', function(err) {
    // Handle error
    console.log('Error' + err);
});

// End the request
request.end();
