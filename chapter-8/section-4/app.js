/**
 * Return the nearest alternative fuel stations
 * within a distance of a given location.
 * Pass the API key into the X-Api-Key header
 *
 * @summary   Closest Alternative Fuel Station
 *
 * @link      https://api.data.gov/docs/api-key/
 * @since     8.1.1
 */

// Require the HTTPS module
var https = require('https');
var qs = require('querystring'); 

// Sign up here https://api.data.gov/signup/ for your own KEY and Secret

/* API Key @secure-keys */
var api_key = "** Replace With Your Value **";

// Sample URL
// https://api.data.gov/nrel/alt-fuel-stations/v1/nearest.json?api_key=####&location=Missoula+MT
var host = api_key+':@api.data.gov';

// API Parameters:
var path = '/nrel/alt-fuel-stations/v1/nearest.json?' + qs.stringify({location: 'Missoula+MT', limit: 1});

// Setup the request option values
var options = {
  host: host,
  // port: 443, // 443 Secure URL, 80 Standard URL
  // path: path,
  // method: 'GET',
  // protocol: 'https:', 
  url: 'https://yahoo.com/'
};

// Begin the request
var request = https.request(options, function(res) {
  // removed, to just show body content
  // console.log('STATUS: ' + res.statusCode);
  // console.log('HEADERS: ' + JSON.stringify(res.headers));
  // res.setEncoding('utf8');
  res.on('data', function (chunk) {
    console.log('' + chunk); // empty string to cast to string
  });
});

// Handle any errors
request.on('error', function(err) {
    // Handle error
    console.log('Error' + err);
});

// End the request
request.end();
