/**
 * The Bacon Ipsum JSON API is a REST interface 
 * for generating meaty lorem ipsum text and can 
 * be used by any application. Pass in the following 
 * parameters using an HTTP GET and we'll return 
 * a JSON string array of paragraphs. It can also 
 * be accessed via https.
 *
 * @summary   This app.js outputs random bacom ipsum values.
 *
 * @link      https://baconipsum.com/json-api/
 * @since     8.1.1
 */

// Require the HTTPS module
var https = require('https');
var qs = require('querystring'); 

// Sign up here https://api.data.gov/signup/ for your own KEY and Secret

/* API Key @secure-keys */
var api_key = "** Replace With Your Value **";

// Sample URL
// https://api.data.gov/nrel/alt-fuel-stations/v1/nearest.json?api_key=####&location=Missoula+MT
var host = 'api.data.gov';

// API Parameters:
var path = '/nrel/alt-fuel-stations/v1/nearest.json?' + qs.stringify({api_key: api_key, location: 'Missoula+MT'});

// Setup the request option values
var options = {
  host: host,
  port: 443, // 443 Secure URL, 80 Standard URL
  path: path,
  method: 'GET',
  protocol: 'https:'
};

// Begin the request
var request = https.request(options, function(res) {
  // removed, to just show body content
  // console.log('STATUS: ' + res.statusCode);
  // console.log('HEADERS: ' + JSON.stringify(res.headers));
  // res.setEncoding('utf8');
  res.on('data', function (chunk) {
    console.log('' + chunk); // empty string to cast to string
  });
});

// Handle any errors
request.on('error', function(err) {
    // Handle error
    console.log('Error' + err);
});

// End the request
request.end();
