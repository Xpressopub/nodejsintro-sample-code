// initialize gulp
var gulp = require('gulp');

// copies all css files from src to dist folder
gulp.task('css', function() {
	return gulp.src('./src/css/**.*')
        .pipe(gulp.dest('./dist/css'));
});

// runs 'css' task 
gulp.task('default', [ 'css']);
